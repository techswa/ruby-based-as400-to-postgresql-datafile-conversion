# encoding: utf-8
# Prep the source file for importing into postgres

source_file = 'trec-test-clean.txt'
target_file = 'trec-test-prepped.txt'


  #Delete last output_file if it exists
  if File.file?(target_file)
    File.delete(target_file)
  end
  
  #Open the target file
  output_file = File.open(target_file, 'w')
  
  # Append the current line to the file
  def append_line(line, output_file)
    open(output_file, 'a') do |f|
      f.puts line
    end
  end
  
  row = '' # holds row data
  File.open(source_file,"r") do |file|
    puts '====== Beginning processing ====='
    break if file.lineno == 2

    
    # Process each line  
    file.each_line do |line|
      
      line.split(/\t/).each do |field|
        #Seperate fields with a comma unless first field
        if row != ''
          row << ','
        end
        # add the quoted field to the row    
        row << "\"#{field}\""
      end
      
      append_line row, output_file
      #reset row
      row = ''  
      puts "processing line: #{file.lineno}"
      break if file.lineno == 2
    end  
        
      puts "====== Processed record #{file.lineno} ==========="
      puts ""
      
      #Only process first ten lines
      # break if file.lineno == 1
      
      #After the current line has been processed
      
      
    # end
  end



    
