
source_file = 'trecfile-stripped.txt'
output_file = File.open('trec-test.txt', 'w')


  #Delete last output_file if it exists
  if File.file?(output_file)
    File.delete(output_file)
  end

  # Append the current line to the file
  def append_line(line, output_file)
    open(output_file, 'a') do |f|
      f.puts line
    end
  end

  File.open(source_file,"r") do |file|
    file.each_line do |line|
      
      # Append line to output file
      append_line line, output_file 
      
      #Only process first ten lines
      break if file.lineno == 10
      
    end
  end

    


  

# # Write output file header
# # Write the processed line out to the cleaned file
# #open('myfile.txt', 'a') do |f|
# #  f.puts new_row
# #end
# 
# the_line = []
# in_title = false
# line_index = 0
# row = []
# field = []
# line_index = 0
# comma_pos_current = 0 #Start at the beginning of the line
# start_next_field = 0 #index of the first char of the next field after comma
# 
# IO.foreach(source_file) do |line|
#   # puts "Processing line: " + line_index.to_s
#   if line_index > 0
#     puts line
# 
#   #Get the first field
#     comma_pos_current = line.index(',', comma_pos_current)
# 
#     #check the the next char is a " - this signifies a new field
#     if line[comma_pos_current + 1] = '"'
#       #Grab the contents
#       field << "\"#{line[0..comma_pos_current-1]}\""
# 
#       start_next_field = comma_pos_current + 1 #setup next field
#     end
# 
#   #Get the second field - include existing quotes
#     comma_pos_current = line.index(',', start_next_field)
#     # puts "Start next field: " + start_next_field.to_s
#     # puts "End next field: " + comma_pos_current.to_s
#     field << line[start_next_field..comma_pos_current-1]
#     start_next_field = comma_pos_current + 1 #setup next field
# 
#     #Get the third field - include existing quo25000tes
#     comma_pos_current = line.index(',', start_next_field)
#     # puts "Start next field: " + start_next_field.to_s
#     # puts "End next field: " + comma_pos_current.to_s
#     field << line[start_next_field..comma_pos_current-1]
#     start_next_field = comma_pos_current + 1 #setup next field
# 
#     #Get the fourth field - include existing quotes
#     comma_pos_current = line.index(',', start_next_field)
#     # puts "Start next field: " + start_next_field.to_s
#     # puts "End next field: " + comma_pos_current.to_s
#     field << line[start_next_field..comma_pos_current-1]
#     start_next_field = comma_pos_current + 1 #setup next field
# 
#     #Get the fifth field - include existing quotes
#     comma_pos_current = line.index(',', start_next_field)
#     # puts "Start next field: " + start_next_field.to_s
#     # puts "End next field: " + comma_pos_current.to_s
#     field << line[start_next_field..comma_pos_current-1]
#     start_next_field = comma_pos_current + 1 #setup next field
# 
#     #Get the sixth field - include existing quotes
#     comma_pos_current = line.index(',', start_next_field)
#     # puts "Start next field: " + start_next_field.to_s
#     # puts "End next field: " + comma_pos_current.to_s
#     field << line[start_next_field..comma_pos_current-1]
#     start_next_field = comma_pos_current + 1 #setup next field
# 
#     #Get the seventh field - include existing quotes
#     comma_pos_current = line.index(',', start_next_field)
#     # puts "Start next field: " + start_next_field.to_s
#     # puts "End next field: " + comma_pos_current.to_s
#     field << line[start_next_field..comma_pos_current-1]
#     start_next_field = comma_pos_current + 1 #setup next field
# 
#     # Date Get the eighth field - ad quotes and adjust date format
#     comma_pos_current = line.index(',', start_next_field)
#     # puts "Start next field: " + start_next_field.to_s
#     # puts "End next field: " + comma_pos_current.to_s
#     field << '"' + line[start_next_field..comma_pos_current-1] + '"'
#     start_next_field = comma_pos_current + 1 #setup next field
# 
#     # Date Get the nineth field - ad quotes and adjust date format
#     comma_pos_current = line.index(',', start_next_field)
#     # puts "Start next field: " + start_next_field.to_s
#     # puts "End next field: " + comma_pos_current.to_s
#     field << '"' + line[start_next_field..comma_pos_current-1] + '"'
#     start_next_field = comma_pos_current + 1 #setup next field
# 
#     # Y Get the tenth field - include existing quotes
#     comma_pos_current = line.index(',', start_next_field)
#     # puts "Start next field: " + start_next_field.to_s
#     # puts "End next field: " + comma_pos_current.to_s
#     field << line[start_next_field..comma_pos_current-1]
#     start_next_field = comma_pos_current + 1 #setup next field
# 
#     # ========= Get the company name ======================
#     #Company name - problem comma prior to Inc or LLC
#     #Get the eleventh field - include existing quotes
#     comma_pos_current = line.index('"', start_next_field+1)
#     # puts 'Next field: ' + line[start_next_field+3]
#     field << line[start_next_field..comma_pos_current - 1] + '"'
#     start_next_field = comma_pos_current + 2 #setup next field
# 
# 
#     # ======= Company NMLS number =============================
#     #Get the twelveth field - add quotes
#     comma_pos_current = line.index(',', start_next_field+1)
#     # Check for the "," - but missing the end quote delimiter
#     # puts line[comma_pos_current]
#     # puts "Start next field: " + start_next_field.to_s
#     # puts "End next field: " + comma_pos_current.to_s
#      field << '"' + line[start_next_field..comma_pos_current-1] + '"'
#      start_next_field = comma_pos_current + 1 #setup next field
# 
#     #Organisation type
#    comma_pos_current = line.index(',', start_next_field)
#    # puts "Start next field: " + start_next_field.to_s
#    # puts "End next field: " + comma_pos_current.to_s
#    field << line[start_next_field..comma_pos_current-1]
#    start_next_field = comma_pos_current + 1 #setup next field
# 
#    #Org NMLS number
#    comma_pos_current = line.index(',', start_next_field)
#    # puts "Start next field: " + start_next_field.to_s
#    # puts "End next field: " + comma_pos_current.to_s
#    field << '"' + line[start_next_field..comma_pos_current-1] + '"'
#    start_next_field = comma_pos_current + 1 #setup next field
# 
#    # ========= Get the Address 1 - comma issue ======================
#    #Company name - problem comma prior to Inc or LLC
#    #Get the eleventh field - include existing quotes
#    comma_pos_current = line.index('"', start_next_field+1)
#    # puts 'Next field: ' + line[start_next_field+3]
#    field << line[start_next_field..comma_pos_current - 1] + '"'
#    start_next_field = comma_pos_current + 2 #setup next field
# 
#    # ========= Get the Address 2 - comma issue ======================
#    #Company name - problem comma prior to Inc or LLC
#    #Get the eleventh field - include existing quotes
#    comma_pos_current = line.index('"', start_next_field+1)
#    # puts 'Next field: ' + line[start_next_field+3]
#    field << line[start_next_field..comma_pos_current - 1] + '"'
#    start_next_field = comma_pos_current + 2 #setup next field
# 
#    # ========= Get City ======================
#    # Date Get the sixteenth field - ad quotes and adjust date format
#    comma_pos_current = line.index(',', start_next_field)
#    field << line[start_next_field..comma_pos_current-1]
#    start_next_field = comma_pos_current + 1 #setup next field
# 
#    # ========= Get State ======================
#    # Date Get the sixteenth field - ad quotes and adjust date format
#    comma_pos_current = line.index(',', start_next_field)
#    field << line[start_next_field..comma_pos_current-1]
#    start_next_field = comma_pos_current + 1 #setup next field
# 
#    # ========= Get Zipcode ======================
#    # Date Get the sixteenth field - ad quotes and adjust date format
#    comma_pos_current = line.index(',', start_next_field)
#    field << line[start_next_field..comma_pos_current-1]
#    start_next_field = comma_pos_current + 1 #setup next field
# 
#    # ========= Get Phone ======================
#    # Date Get the sixteenth field - ad quotes and adjust date format
#    comma_pos_current = line.index(',', start_next_field)
#    field << line[start_next_field..comma_pos_current-1]
#    start_next_field = comma_pos_current + 1 #setup next field
# 
#    # ========= Get Fax ======================
#    # Date Get the sixteenth field - ad quotes and adjust date format
#   #  comma_pos_current = line.index(',', start_next_field)
#   field << '\"123\"'
#   #  start_next_field = comma_pos_current + 1 #setup next field
# 
#   end
#   #print the fields
#   # puts field[0]
#   # puts field[1]
#   # puts field[2]
#   # puts field[3]
#   # puts field[4]
#   # puts field[5]
#   # puts field[6]
#   # puts field[7] #date
#   # puts field[8] #date
#   # puts field[9] # Y
#   # puts field[10] # ???
#   # puts field[11] # Business name
#   # puts field[12] # Business NMLS
#   # puts field[13] # Business NMLS
#   # puts field[14] # Address 1
#   # puts field[15] # Address 2
#   # puts field[16] # City
#   # puts field[17] # State
#   # puts field[18] # Zipcode
#   # puts field[19] # Phone
#   # puts field[20] # Fax
#   #Create the new line to be used
#    new_row = "#{field[0]},#{field[1]},#{field[2]},#{field[3]},#{field[4]},#{field[5]},#{field[6]},#{field[7]},#{field[8]},#{field[9]},#{field[10]},#{field[11]},#{field[12]},#{field[13]},#{field[14]},#{field[15]},#{field[16]},#{field[17]},#{field[18]},#{field[19]}"
#   # Write the processed line out to the cleaned file
#   open('output.csv', 'a') do |f|
#     f.puts new_row
#   end
#   #Reset for the line to be processed
#   line_index = line_index + 1
#   comma_pos_current = 0 #Reset char index
#   field = [] #Reset fields for the new line
# 
#   # new_row = "#{field[0]},#{elements[1]},#{elements[2]},#{elements[3]},#{elements[4]},#{elements[5]},#{elements[6]},#{elements[7]},#{elements[8]},#{elements[9]},#{elements[10]},#{elements[11]},#{elements[12]},#{elements[13]},#{elements[14]},#{elements[15]},#{elements[16]},#{elements[17]},#{elements[18]},#{elements[19]},#{elements[20]}"
# 
# end
# # out_file.close
