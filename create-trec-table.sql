﻿CREATE UNLOGGED TABLE licenses
(
  id serial NOT NULL,
  license_type character varying(4),
  license_number character varying(10),
  full_name character varying(64),
  suffix character varying(4),
  license_status character varying(2),
  original_license_date date,
  license_expiration_date date,
  education_status character varying(1),
  mce_status character varying(1),
  designated_supervisor_flag character varying(1),
  phone_number character varying(12),
  email_address character varying(100),
  mailing_address_1 character varying(54),
  mailing_address_2 character varying(40),
  mailing_address_3 character varying(40),
  mailing_address_city character varying(20),
  mailing_address_state_code character varying(3),
  mailing_address_zip_code character varying(10),
  mailing_address_county_code character varying(3),
  physical_address_1 character varying(54),
  physical_address_2 character varying(40),
  physical_address_3 character varying(40),
  physical_address_city character varying(20),
  physical_address_state_code character varying(3),
  physical_address_zip_code character varying(10),
  physical_address_county_code character varying(3),
  related_license_type character varying(4),
  related_license_number character varying(10),
  related_license_full_name character varying(64),
  related_license_suffix character varying(4),
  relationship_start_date date,
  agency_identifier character varying(10),
  last_name_first character varying(64),
  CONSTRAINT licenses_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE licenses
  OWNER TO swa;
