# README #

## Ruby based database file conversion and import into PostgresSQL ##

Convert & Import State of Texas Real Estate Agent Database to a PostgreSQL server for web access.

The provided source file includes incompatible control characters that have to be stripped out.  The source file is then converted from Tab delimited to CSV for importing into a PostgreSQL table.

* Abridged sample files included