﻿SELECT 
  licenses.license_type,
  licenses.license_number, 
  licenses.full_name, 
  licenses.original_license_date, 
  licenses.mailing_address_city, 
  licenses.mailing_address_zip_code, 
  licenses.phone_number,
  licenses.related_license_number
FROM 
  public.licenses
WHERE
  licenses.license_type = 'SALE'
AND
  UPPER(licenses.mailing_address_city) = UPPER('dallas')
OR
  UPPER(licenses.mailing_address_city) = UPPER('carrollton')
OR
  UPPER(licenses.mailing_address_city) = UPPER('frisco')    
AND
  licenses.original_license_date >= '2016/01/01' AND licenses.original_license_date < '2013/12/31'

ORDER BY
  licenses.original_license_date;